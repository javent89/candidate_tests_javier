// JS stuff

$(function() {
    console.log( "Go! go! go!" );
});

$(document).ready(function() {
  $('.popup-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#name',
    callbacks: {
      beforeOpen: function() {
        if($(window).width() < 700) {
          this.st.focus = false;
        } else {
          this.st.focus = '#name';
        }
      }
    }
  });
});
