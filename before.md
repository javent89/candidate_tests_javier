We expect some knowledge of bootstrap 3. And we require you to use the BEM metodology. Also for some part of the test you'll have to use the Magnific popup plugin and, of course, jQuery.

Recomended links:
- [Bootstrap 3](http://getbootstrap.com/getting-started/)
- [BEM metodology](https://en.bem.info/method/definitions/)
- [Magnific popup](http://dimsemenov.com/plugins/magnific-popup/)
