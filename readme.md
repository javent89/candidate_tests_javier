# Welcome
This test consists in two parts and a bonus point. We will evaluate the markup and the fact that it actually works in different screen resolutions. The design doesn't have to be pixel perfect.


 ### Rules
 - You have 3 hours to complete part 1 and 2
 - For the bonus point you have half a hour more
 - Adhere to the [BEM metodology](https://en.bem.info/method/definitions/)
 - Crossbrowser compatibility
 - Make it valid code (by W3C)
 - Do it with love


### Instructions
- You can use [bootstrap ](http://getbootstrap.com/getting-started/#download-cdn), no need to create a new theme, the default it's just OK
- Trust your gut with the colors, font types and sizes


### Delivery
You can [download the code](https://bitbucket.org/zankyou/candidate_tests/downloads) for the test in the download section or fork the repo if you like it. When you finish sent it back to us by email or creating a pull request from bitbucket to this repo.

## Part 1

Make a hero module to show some feature of our product.

### Definition
A hero module that has a "background" image and some text flying over. The text occupies the space as follows: 1/3 in desktop, 1/2 in tablet and everything in mobile. In mobile we change the image to another one smaller.

Notes:
- The module adapts to the image
- The text has a dynamic shadow in the background to improve readbility
- In the mobile version we use the smaller image and reduce the text size for fitting it into a small screen

### Screenshots
Desktop
![part1](designs/part-1--desktop.jpg)


## Part 2

We need a module to show the opinions of our clients. It has two parts, a resume of all the opinions and the opinion itself.

### Definition


Notes:
- Use the image placeholder for the stars

### Screenshots
Desktop
![part2](designs/part-2--desktop.png)

Mobile
![part2](designs/part-2--mobile.png)



## Bonus point

A modal window to contact our providers. It consists in a header section and a form. In desktop use two columns for the form and full width in mobile.

### Definition

Create a button that opens a modal window like the design.

Notes:
 - Use [magnific popup](http://dimsemenov.com/plugins/magnific-popup/)

### Screenshots
Desktop
![part3](designs/part-3--desktop.jpg)

Mobile
![part3](designs/part-3--mobile.jpg)

---

If you have any doubts or wanna ask something you can email david.rodriguez@zankyou.com anytime.

Have fun!
